<?php
/**
 * Created by : PhpStorm
 * User: godjarvis
 * Date: 2022/6/2
 * Time: 14:52
 */

namespace ZhangWan;


class HelloWorldEchoService
{
    public static function echo(): string
    {
        echo $str = 'hello world!';
        return $str;
    }
}